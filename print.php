<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>PRINT Each Page A Function</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/preprint.css">
    <link rel="stylesheet" href="fonts/stylesheet.css">
  </head>
  <body>
    <div class="unsorted">
      <section class="pageframe cover">
        <img class="textframe rb" src="img/cover.svg" alt="">
        <img class="textframe public" src="img/public-cover.svg" alt="">
      </section>
      <section class="pageframe blank"></section>
    <?php
      $content =  $_POST["htmlpages"];
      echo $content;
      // $timestamp = time();
      // $archiveFile = fopen("archives/".$timestamp.".html", "w") or die("Unable to open file!");
      // fwrite($archiveFile, $content);
      // fclose($archiveFile);
    ?>
    <section class="pageframe glossary-page" data-page="1">
    <div class="textframe">
    <div class="glossary"></div>
    </div>
    </section>
    <section class="pageframe blank"></section>
    <section class="pageframe backcover">
    <div class="textframe">
      <p class="description">This publication was made in a web browser with free (open) and standard tools such as HTML, CSS, and the Paper.js library. You can use, copy and modify the drawing interface on the page: https://raphaelbastide.com/epaf</p>

      <div class="credits rb">
        <p>Drawings and programs by Raphaël Bastide</p>
        <p>Published and printed by LeMégot éditions at L’École in Bagnolet in May 2019</p>
        <p>www.lemegot.com</p>
      </div>
      <div class="credits public">
        <p>Drawings by _________________, programs by Raphaël Bastide</p>
        <p>Published and printed by _________________ at _______________ in 20__</p>
        <p>Project supported by LeMégot éditions - www.lemegot.com</p>
      </div>
      <img class="ppp" src="img/ppp.svg" alt="">
    </div>
    </section>
    </div>
    <main>
  </main>
  </body>
  <script type="text/javascript" src="js/print.js"></script>
  <!-- <script type="text/javascript" src="js/paged-polyfill.js"></script> -->
</html>
