var d = document,
    body = d.getElementsByTagName("body")[0],
    downloadBtn = d.getElementById("downloadsvg"),
    resetBtn = d.getElementById("reset"),
    frameBoxes = d.querySelectorAll('.frame'),
    svgs = d.querySelectorAll('.frame svg'),
    toolBtns = d.querySelectorAll('.tool'),
    drawingList = d.getElementById('drawinglist'),
    pageList = d.getElementById('pagelist'),
    oneSVG = d.querySelectorAll('.frame svg')[0],
    splash = d.querySelectorAll('.splash')[0],
    splashBtn = d.querySelectorAll('.splashBtn'),
    artzone = d.getElementById('mainartzone'),
    toolname = d.getElementById('toolname'),
    sessionEnded = false,
    frameImgNbrHor = 9,
    frameImgNbrvert = 11,
    // artzoneWidth = 180,
    // artzoneHeight = 220,
    artzoneWidth = 650,
    artzoneHeight = 775,
    strokesInFolder = 7;

if (Cookies.get('firstVisit') != 'true') {
  console.log('t');
  Cookies.set('firstVisit', true, { expires: 1 });
  body.classList.add('info')
}

downloadBtn.addEventListener('click', function(){
  var svg = project.exportSVG({ asString: true });
  var svgBlob = new Blob([svg], {type:"image/svg+xml;charset=utf-8"})
  var svgUrl = URL.createObjectURL(svgBlob)
  downloadBtn.href = svgUrl
  downloadBtn.download = "drawing.svg"
})
resetBtn.addEventListener('click', function(){
  var currentTool = app[Object.keys(app)[globals.nextToolIndex]]
  currentTool.activate()
  if (currentTool.name == "endWithFirstShape") {
    currentTool.firstPath = true
  }else if(currentTool.name == "incrementedWeight"){
    currentTool.s = 1
  }else if(currentTool.name == "frameOfVersions"){
    globals.thumb = 1
    for (var i = 0; i < frameBoxes.length; i++) {
      frameBoxes[i].innerHTML = ""
    }
  }else if(currentTool.name == "clearer"){
    currentTool.l = 15
    currentTool.r = 1
  }else if(currentTool.name == "brushIsFirstShape"){
    currentTool.firstPath = true
  }else if(currentTool.name == "inhabited"){
    importStroke()
  }
  project.clear()
})
for (var i = 0; i < splashBtn.length; i++) {
  console.log(i);
  splashBtn[i].addEventListener('click', function(){
    toggleSplash()
  })
}
function impose(){
//   var pages = d.querySelectorAll('#drawinglist .pageframe')
//   var pageIds = []
//   var newOrder = []
//   for (var i = 0 ; i < pages.length; i++) {
//     var p = pages[i].getAttribute('data-page')
//     pageIds.push(p)
//   }
//   totalPages = pageIds.length
//   console.log(totalPages);
//   for (var i = 0; i < totalPages / 2; i++) {
//     var f = pageIds.shift()
//     var l = pageIds.pop()
//     if (i % 2) { // odd
//       newOrder.push(f,l)
//     }else { // even
//       newOrder.push(l,f)
//     }
//   }
//   for (var i = 0; i < newOrder.length; i++) {
//     if (i!= newOrder.length && i%2 == 0) {
//       var jump = d.createElement('div')
//       jump.classList.add('pageBreak')
//       pageList.appendChild(jump)
//     }
//     var pageid = newOrder[i]
//     var drawing = d.querySelectorAll('[data-page="'+newOrder[i]+'"]')[0]
//     page = drawing.cloneNode(true)
//     pageList.appendChild(page)
//   }
}
function toggleSplash(){
  if (body.classList.contains('info')) {
    body.classList.remove('info')
  }else {
    body.classList.add('info')
  }
}
function importStroke(){
  var r =  Math.floor((Math.random() * strokesInFolder) + 1);
  project.importSVG('strokes/'+r+'.svg')
}
function createGauge(){
  var rect = new Rectangle(0, 20, view.size._width, view.size._height);
  project.rect.fillColor = "black"
}
function exportPages(){
  var inputTarget = d.getElementById('htmlpages')
  inputTarget.innerHTML = drawinglist.innerHTML
}
function createCover(){
  cover = pageList.cloneNode(true)
  cover.setAttribute('class','cover')
  pageList.appendChild(cover)
}

d.addEventListener("keydown", function(e){
  if(e.keyCode == 13 && !sessionEnded){
    globals.nextDrawing()
  }
});

// Functions are set in the window.globals scope to be used within the paperjs env.
window.globals = {
  thumb : 1,
  imgPerSide : [0, 0, 0, 0],
  svgToThumb : function (){ // This function archive the states of the drawing to its frame
    var svg = project.exportSVG()
    var initImgPerSideHor = frameImgNbrHor-1 // first number is the grid space set in css
    var initImgPerSideVert = frameImgNbrvert-1
    var ips = globals.imgPerSide
    if (globals.thumb <= initImgPerSideHor * 1) {
      frameBoxes[0].appendChild(svg)
      ips[0]++
    }else if (globals.thumb <= initImgPerSideHor + initImgPerSideVert) {
      frameBoxes[1].appendChild(svg)
      ips[1]++
    }else if (globals.thumb <= initImgPerSideHor * 2 + initImgPerSideVert) {
      frameBoxes[2].appendChild(svg)
      ips[2]++
    }else if (globals.thumb < (initImgPerSideVert + initImgPerSideHor + 1) * 2) {
      frameBoxes[3].appendChild(svg)
      ips[3]++
    }else {
      frameBoxes[3].appendChild(svg)
      ips[3]++
      for (var i = 0; i < ips.length; i++) { // for each frame side
        if (ips[i] != 0 && ips[i] - ips[i-1] >= 1) { // If not the 1st side and has a different amount of img compared to the side before (top,r,b,l)
          var overflow = frameBoxes[i].firstChild
          frameBoxes[i-1].appendChild(overflow) // move the surplus img to the prev side
          ips[i]-- // decrease current number of images
          ips[i-1]++ // increase the number of img of the previous frame side
        }
      }
    }
    svg.setAttribute('viewBox', '0 0 '+artzoneWidth+' '+artzoneHeight+'')
    svg.removeAttribute('width')
    svg.removeAttribute('height')
    globals.thumb++
  },
  // importStroke : function(t){
  //   console.log(t);
  //   var r =  Math.floor((Math.random() * strokesInFolder) + 1);
  //   project.importSVG('strokes/'+r+'.svg')
  // },
  nextToolIndex : 0,
  nextDrawing : function(){
    var svg = project.exportSVG()
    var page = d.createElement('section')
    var nextTool = app[Object.keys(app)[globals.nextToolIndex + 1]]
    pageArtZone = artzone.cloneNode(true)
    pageArtZone.setAttribute('class','artzone')
    pageArtZone.setAttribute('id','')
    artzone.setAttribute('data-tool', '')
    artzone.setAttribute('style', '') // reset style (blur)
    page.setAttribute('class', 'pageframe tool')
    page.setAttribute('data-page', globals.nextToolIndex + 2)
    // var tools = app[Object.keys(app)[globals]]
    // for (var i = 0; i < tools.length; i++) {
    //   tools[i].remove
    // }
    if (typeof nextTool !== 'undefined') {
      toolname.innerHTML = nextTool.name+"()"
      console.log(nextTool.name);
      if (nextTool.name == "inhabited") {
        importStroke()
      }
      globals.nextToolIndex++
      pageArtZone.appendChild(svg)
      page.appendChild(pageArtZone)
      drawingList.appendChild(page)
      svg.classList.add('canvas')
      svg.setAttribute('viewBox', '0 0 '+artzoneWidth+' '+artzoneHeight+'')
      project.clear()
      nextTool.activate()
    }else {
      pageArtZone.appendChild(svg)
      page.appendChild(pageArtZone)
      drawingList.appendChild(page)
      svg.classList.add('canvas')
      sessionEnded = true
      d.body.classList.add('done')
      console.log('finish');
    }
  },
  prePrint : function(){
    // impose()
    exportPages()
    setTimeout(function(){d.getElementById('printform').submit() }, 500);
  }
};
