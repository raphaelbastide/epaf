var path,
    tailLimit = 100,
    randrandomThickness = 100,
    fontSize = '8px',
    fontFamily = 'arial',
    artzone = document.getElementById('mainartzone')

function onMouseDown(event) {
  path = new Path();
  path.strokeColor = 'black';
  path.add(event.point);
}
function setData(name, progTime, drawingTime, desc){
  artzone.setAttribute('data-tool',name)
  artzone.setAttribute('data-progTime',progTime)
  artzone.setAttribute('data-drawigTime',drawingTime)
  artzone.setAttribute('data-desc',desc)
}

var size = view.size;

window.app = {
  soft: new Tool({
    name:"superSmooth", progTime:"10min", drawingTime:"2min",
    desc:"All edges are smoothened.",
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
    },
    onMouseDrag: function(event) {
      path.add(event.point);
    },
    onMouseUp: function(event){
      var segmentCount = path.segments.length;
      path.simplify(1000);
    }
  }),

  audioTrembling: new Tool({
    name:"audioTrembling", progTime:"4h20", drawingTime:"2min",
    desc:"The strokes are excited by the volume recorded by a microphone. ",
    mul :1,
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
    },
    onMouseDrag: function(event) {
      var step = event.delta.rotate(90 * app.audioTrembling.mul);
      step.length = vol*10;
      var nvol = vol*10
      path.add({
        point: event.point + step,
        handleIn: -event.delta * nvol,
        handleOut: event.delta * nvol
      });
      app.audioTrembling.mul *= -1;
    },
    // onMouseUp: onMouseUp
  }),
  dissect: new Tool({
    name:"dissect", progTime:"1h40", drawingTime:"1min",
    desc:"The shapes of the drawing are sorted and allocated to the corners of the frame. Like a frog in a lab, we can see what is inside.",
    t: 0,
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
      path.strokeWidth = '8';
      path2 = new Path();
      path2.strokeWidth = '1';
      path2.strokeColor = 'black';
    },
    onMouseDrag: function(event) {
      path.lineTo(event.point);
      var vw = view.size._width
      var vh = view.size._height
      if (app.dissect.t == 0) {
        path2.lineTo(vw - vw * .1 + event.point.x * .1,vh -vh * .1 + event.point.y * .1);
      }else if (app.dissect.t == 1) {
        path2.lineTo(event.point.x * .1,vh -vh * .1 + event.point.y * .1);
      }else if (app.dissect.t == 2) {
        path2.lineTo(event.point.x * .1, event.point.y * .1);
      }else if (app.dissect.t == 3) {
        path2.lineTo(vw - vw * .1 + event.point.x * .1, event.point.y * .1);
      }
    },
    onMouseUp: function(event){
      if (app.dissect.t == 3) {
        app.dissect.t = 0
      }else {
        app.dissect.t++
      }
    }
  }),

  bounceInside: new Tool({
    name:"bounceInside", progTime:"1h10", drawingTime:"3min",
    desc:"The drawing is restricted to the frame and bounces off its borders.",
    t: 0,
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
      path.strokeWidth = '1';
    },
    onMouseDrag: function(event) {
      var vw = view.size._width
      var vh = view.size._height
      if (event.point.x > vw) {
        nx = vw - (event.point.x - vw)
      }else if (event.point.x < 0) {
        nx = - event.point.x
      }else {
        nx = event.point.x
      }
      if (event.point.y > vh) {
        ny = vh - (event.point.y - vh)
      }else if (event.point.y < 0) {
        ny = - event.point.y
      }else {
        ny = event.point.y
      }
      path.lineTo(nx, ny);
    }
  }),
  randomThickness: new Tool({
    name:"randomThickness", progTime:"2h30", drawingTime:"10min",
    desc:"Beginning a stroke, I don't know what width it will get, and it will also change randomly along the line.",
    r : 0,
    points : 0,
    weight : '1',
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
      path.strokeWidth = event.count % 2 ? '1' : '20';
      path.add(event.point);
    },
    onMouseDrag: function(event) {
      if (app.randomThickness.points == app.randomThickness.r) {
        path.add(event.point);
        path.simplify(1);
        path = new Path();
        path.strokeColor = 'black';
        if (app.randomThickness.weight == "1") {
          app.randomThickness.weight = '20'
        }else {
          app.randomThickness.weight = '1'
        }
        path.strokeWidth = app.randomThickness.weight;
        app.randomThickness.points = 0;
        path.add(event.point);
        app.randomThickness.r = Math.floor(Math.random() * randrandomThickness) +1
      }else {
        path.add(event.point);
        app.randomThickness.points++
      }
    }
  }),

  limitedLength: new Tool({
    name:"limitedLength", progTime:"50min", drawingTime:"2min",
    desc:"If a stroke exceeds a given length, its tail is shortened.",
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
    },
    onMouseDrag: function(event) {
      path.add(event.point);
      if (path.segments.length >= tailLimit) {
        path.removeSegment(0);
      }
    },
    onMouseUp: function(event){
      path.simplify();
    }
  }),
  incrementedWeight: new Tool({
    name:"incrementedWeight", progTime:"20min", drawingTime:"1min",
    desc:"Straight lines gain width each time a new one is made.",
    handle: null,curPoint:null, prevPoint:null, curHandleSeg:null,
    s: 1,
    p : new Path(),
    onMouseDown: function(event){
      setData(this.name, this.progTime, this.drawingTime, this.desc)
      app.incrementedWeight.p.strokeColor = 'black';
      app.incrementedWeight.p.strokeWidth = app.incrementedWeight.s ++;
      app.incrementedWeight.p.strokeCap = 'round'
      app.incrementedWeight.p.add(event.point);
      app.incrementedWeight.p = new Path()
      app.incrementedWeight.p.add(event.point);
    },
    onMouseDrag: function(event) {
      // path.lineTo(event.point);
    },
    onMouseUp: function(event){
    }
  }),

  endWithFirstShape: new Tool({
    name:"endWithFirstShape", progTime:"2h", drawingTime:"3min",
    desc:"The first shape is added at the end of all the following strokes. Like a gentle rhyme.",
    firstPath: true,
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
    },
    onMouseDrag: function(event) {
      path.add(event.point)
    },
    onMouseUp: function(event){
      var clone
      if (app.endWithFirstShape.firstPath) {
        first = path;
        app.endWithFirstShape.firstPath = false
      }else {
        first.clone()
        first.position = event.point
      }
    }
  }),
  frameOfVersions: new Tool({
    name:"frameOfVersions", progTime:"7h43", drawingTime:"5min",
    desc:"Each step of the drawing process is added to a frame, like a image in its own archive.",
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
      path.strokeWidth= '4';
    },
    onMouseDrag: function(event) {
      path.add(event.point)
    },
    onMouseUp: function(event){
      var segmentCount = path.segments.length;
      // path.simplify(1000);
      globals.svgToThumb()
    }
  }),

  clearer: new Tool({
    name:"clearer", progTime:"1h12", drawingTime:"2min",
    desc:"The blurry drawing area gets sharper while drawing. Inverted macular degeneration.",
    l : 15,
    r : 1,
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
      var c = document.querySelectorAll('[data-tool="clearer"]')[0]
      // var s = c.children.getElementsByTagName('svg')[0]
      c.setAttribute('style', 'filter:blur('+app.clearer.l+'px);')
      if (app.clearer.r % 4 == 0 && app.clearer.l >= 1) {
        app.clearer.l--
      }
      app.clearer.r++
    },
    onMouseDrag: function(event) {
      path.add(event.point);
    },
    onMouseUp: function(event){
      var segmentCount = path.segments.length;
    }
  }),
  crispAngles: new Tool({
    name:"crispAngles", progTime:"1h02", drawingTime:"4min",
    desc:"Curved lines are replaced by ornamental glitches. I must admit that’s a bug I kept.",
    handle: null,curPoint:null, prevPoint:null, curHandleSeg:null,
    checkValues : function() {
      var min = 10 * 2;
      if (5 < min){min = 5};
      app.crispAngles.handle = 10 * Numerical.KAPPA;
    },
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
      path.strokeWidth = '2';
      path.strokeCap = 'round'
      path.add(event.point)
      if (app.crispAngles.prevPoint !== 'null') {
        app.crispAngles.prevPoint = path.firstSegment.point;
      }
      app.crispAngles.curPoint = path.lastSegment.point;
      app.crispAngles.curHandleSeg = null;
    },
    onMouseDrag: function(event){
      var point = event.point
      var diff = (point - app.crispAngles.prevPoint).abs()
      if (diff.x < diff.y) {
        app.crispAngles.curPoint.x = app.crispAngles.prevPoint.x;
        app.crispAngles.curPoint.y = point.y;
      } else {
        app.crispAngles.curPoint.x = point.x;
        app.crispAngles.curPoint.y = app.crispAngles.prevPoint.y;
      }
      var normal = app.crispAngles.curPoint - app.crispAngles.prevPoint;
      normal.length = 1;
      if (app.crispAngles.curHandleSeg) {
        app.crispAngles.curHandleSeg.point = app.crispAngles.prevPoint + (normal * 10);
        app.crispAngles.curHandleSeg.handleIn = normal * -app.crispAngles.handle;
      }
      var minDiff = Math.min(diff.x, diff.y);
      if (minDiff > 3) {
        var point = app.crispAngles.curPoint - (normal * 10);
        var segment = new Segment(point, null, normal * app.crispAngles.handle);
        path.insert(path.segments.length - 1, segment);
        app.crispAngles.curHandleSeg = path.lastSegment;
        // clone as we want the unmodified one:
        app.crispAngles.prevPoint = app.crispAngles.curHandleSeg.point.clone();
        path.add(app.crispAngles.curHandleSeg);
        app.crispAngles.curPoint = path.lastSegment.point;
      }
    }
  }),

  inhabited: new Tool({
    name:"inhabited", progTime:"2h27", drawingTime:"1min",
    desc:"There is a ready-made shape in the drawing area, I chose to complete it.",
    // import: importStroke(this),
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
    },
    onMouseDrag: function(event){
      path.add(event.point)
    },
    onMouseUp: function(event){
    }
  }),
  brushIsFirstShape: new Tool({
    name:"brushIsFirstShape", progTime:"57min", drawingTime:"1min",
    desc:"The first line becomes the brush for the following stokes.",
    minDistance:10,
    firstPath: true,
    first:new Path(),
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
    },
    onMouseDrag: function(event){
      if (!app.brushIsFirstShape.firstPath) {
        var c = app.brushIsFirstShape.first.clone()
        c.position= event.point
        c.rotate(event.delta.angle + 90)
      }else {
        path.add(event.point)
      }
    },
    onMouseUp: function(event){
      if (app.brushIsFirstShape.firstPath) {
        app.brushIsFirstShape.first = path;
        app.brushIsFirstShape.firstPath = false
      }
    }
  }),

  magicSquares: new Tool({
    name:"magicSquares", progTime:"2h10", drawingTime:"1min",
    desc:"Four squares change the weights of the strokes that are crossing them. Like a minimal palette.",
    weight: '1',
    z1:null,
    z2:null,
    z3:null,
    z4:null,
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
      app.magicSquares.z1 = new Path.Rectangle(view.size._width * .25 -10, view.size.height * .25 -10, 20, 20)
      app.magicSquares.z2 = new Path.Rectangle(view.size._width * .75 -10, view.size.height * .25 -10, 20, 20)
      app.magicSquares.z3 = new Path.Rectangle(view.size._width * .75 -10, view.size.height * .75 -10, 20, 20)
      app.magicSquares.z4 = new Path.Rectangle(view.size._width * .25 -10, view.size.height * .75 -10, 20, 20)
      app.magicSquares.z1.strokeColor = 'black';
      app.magicSquares.z1.strokeWidth = '2';
      app.magicSquares.z2.strokeColor = 'black';
      app.magicSquares.z2.strokeWidth = '6';
      app.magicSquares.z3.strokeColor = 'black';
      app.magicSquares.z3.strokeWidth = '4';
      app.magicSquares.z4.strokeColor = 'black';
      app.magicSquares.z4.strokeWidth = '8';
    },
    onMouseDrag: function(event) {
      var e = event.point
      path.add(event.point)
      app.magicSquares.z1.onMouseLeave = function(event) {
        path = new Path()
        path.strokeWidth = '2';
        path.strokeColor = 'black'
        path.add(event.point)
      }
      app.magicSquares.z2.onMouseLeave = function(event) {
        path = new Path()
        path.strokeWidth = '6';
        path.strokeColor = 'black'
        path.add(event.point)
      }
      app.magicSquares.z3.onMouseLeave = function(event) {
        path = new Path()
        path.strokeWidth = '4';
        path.strokeColor = 'black'
        path.add(event.point)
      }
      app.magicSquares.z4.onMouseLeave = function(event) {
        path = new Path()
        path.strokeWidth = '8';
        path.strokeColor = 'black'
        path.add(event.point)
      }
    }
  }),
  dynamicsStar: new Tool({
    name:"dynamicsStar", progTime:"3h13", drawingTime:"4min",
    desc:"At the center of the frame is a graphic object generated from the drawing speeds.",
    e: 0,
    r: Math.floor(Math.random()*200) + 100,
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
    },
    onMouseDrag: function(event){
      var vw = view.size._width
      var vh = view.size._height
      path.add(event.point)
      onMouseDown(event)
      p = new Path()
      p.add(vw / 2, vh / 2)
      p.add(vw / 2 + event.delta.x, vh / 2 + event.delta.y)
      p.strokeColor = "black"
      // if (app.dynamicsStar.e >= app.dynamicsStar.r) {
      //
      // }
      app.dynamicsStar.e++
    },
  }),

  timestamp: new Tool({
    name:"timeStamp", progTime:"3h30", drawingTime:"10min",
    desc:"Start and end times are added to the stroke.",
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
      var start = new Date()
      var angle = event.delta.angle + 90
      var text = new PointText({position:event.point, rotation:angle });
      var timeStamp = start.getHours()+':'+start.getMinutes()+':'+start.getSeconds()+':'+start.getMilliseconds()
      text.justification = 'center'
      text.fontSize = fontSize
      text.fontFamily = fontFamily
      text.fillColor = 'black'
      text.content = timeStamp
    },
    onMouseDrag: function(event) {
      path.add(event.point)
    },
    onMouseUp: function(event){
      var stop = new Date()
      var angle = event.delta.angle + 90
      var text = new PointText({position:event.point, rotation:angle });
      var timeStamp = stop.getHours()+':'+stop.getMinutes()+':'+stop.getSeconds()+':'+stop.getMilliseconds()
      text.justification = 'center'
      text.fontSize = fontSize
      text.fontFamily = fontFamily
      text.fillColor = 'black'
      text.content = timeStamp
    }
  }),
  fill: new Tool({
    name:"fill", progTime:"1h10", drawingTime:"1min",
    desc:"Every shape is filled.",
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
    },
    onMouseDrag: function(event){
      path.add(event.point)
    },
    onMouseUp: function(event){
      path.fillColor = '#000000'
    }
  }),

  curveless: new Tool({
    name:"curveless", progTime:"1h10", drawingTime:"1min",
    desc:"The lines are straightened.",
    minDistance: 50,
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
    },
    onMouseDrag: function(event) {
      path.lineTo(event.point);
    }
  }),
  duet: new Tool({
    name:"duet", progTime:"1h09", drawingTime:"3min",
    desc:"Two lines follow the gestures and react to its speeds. Butterfly honeymoon.",
    mul: 1,
    minDistance: 10,
    onMouseDown: function(event){
      onMouseDown(event)
      setData(this.name, this.progTime, this.drawingTime, this.desc)
      d = new Path()
      d.strokeColor = "black"
    },
    onMouseDrag: function(event){
      r = Math.floor(Math.random()*40) -40
      rd = Math.floor(Math.random()*20) -20
      d.add(event.point)
      var step = event.delta.rotate(r * app.duet.mul);
      path.add({
          point: event.point + step,
          handleIn: -event.delta * .5,
          handleOut: event.delta * .5
      });
      d.add({
          point: event.point + step,
          handleIn: -event.delta * .5,
          handleOut: event.delta * .5
      });
      app.duet.mul *= -1;
    },
  }),
};
