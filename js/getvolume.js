
var audioContext = null,
 meter = null,
 rafID = null,
 vol = null,
 askForMic = true;

 // document.querySelectorAll('.askformic')[0].addEventListener('click', function() {
 //   audioContext.resume().then(() => {
 //     console.log('Playback resumed successfully');
 //   });
 // });

if (askForMic) {
  window.AudioContext = window.AudioContext || window.webkitAudioContext;
  audioContext = new AudioContext();
  try {
    // monkeypatch getUserMedia
    navigator.getUserMedia =
  	navigator.getUserMedia ||
  	navigator.webkitGetUserMedia ||
  	navigator.mozGetUserMedia;
    navigator.getUserMedia(
    {
      "audio": {
        "mandatory": {

          "googEchoCancellation": "false",
          "googAutoGainControl": "false",
          "googNoiseSuppression": "false",
          "googHighpassFilter": "false"
        },
        "optional": []
      },
    }, gotStream, didntGetStream);
  } catch (e) {
    console.log('getUserMedia threw exception :' + e);
  }
}
function didntGetStream() {
  console.log('Stream generation failed.');
}
var mediaStreamSource = null;
function gotStream(stream) {
  mediaStreamSource = audioContext.createMediaStreamSource(stream);
  meter = createAudioMeter(audioContext);
  mediaStreamSource.connect(meter);
  drawLoop(500);
}

function drawLoop(time) {
  rafID = window.requestAnimationFrame( drawLoop );
  vol = meter.volume
}
