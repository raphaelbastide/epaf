var d = document,
tools = document.querySelectorAll('[data-tool]'),
glossary = document.querySelectorAll('.glossary')[0],
main = d.getElementsByTagName('main')[0];


for (var i = 0; i < tools.length; i++) {
  var name = tools[i].getAttribute('data-tool')
  var progTime = tools[i].getAttribute('data-progTime')
  var drawigTime = tools[i].getAttribute('data-drawigTime')
  var desc = tools[i].getAttribute('data-desc')
  var text = document.createElement('div')
  text.classList.add('text')
  text.innerHTML = "<span class='title'>"+name +"()</span><span class='pt'>Programming time: ~"+progTime+"</span> <span class='dt'>Drawing time: ~"+drawigTime+"</span> <p>"+desc+"</p>"
  glossary.appendChild(text)
}

// Uncomment for imposition
// impose()

function impose(){
  var pages = document.querySelectorAll('[data-page]')
  var pageIds = []
  var newOrder = []
  for (var i = 0 ; i < pages.length; i++) {
    var p = pages[i].getAttribute('data-page')
    pageIds.push(p)
  }
  totalPages = pageIds.length
  for (var i = 0; i < totalPages / 2; i++) {
    var f = pageIds.shift()
    var l = pageIds.pop()
    if (i % 2) { // odd
      newOrder.push(f,l)
    }else { // even
      newOrder.push(l,f)
    }
  }
  for (var i = 0; i < newOrder.length; i++) {
    var pageid = newOrder[i]
    var originalPage = document.querySelectorAll('[data-page="'+pageid +'"]')[0]
    page = originalPage.cloneNode(true)
    main.appendChild(page)
  }
}
