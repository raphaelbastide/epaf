var modeTail = modeAppend = modeNews = modeBuds = 0,
    tailLength = 200,
    budDistance = newsDistance = 50;

function createBud(x,y){
  bud = d.createElementNS('http://www.w3.org/2000/svg', 'path')
  bud.setAttribute("fill","none")
  bud.setAttribute("stroke","red")
  bud.setAttribute('class','bud')
  endCoord = [x + 20,y + 20]
  bud.setAttribute("stroke-width",strokeWidth)
  pathData = "M"+x+" "+y
  console.log(x,y,endCoord[0],endCoord[1]);
  pathData += " L"+endCoord[0]+" "+endCoord[1]
  bud.setAttribute("d",pathData)
  svg.appendChild(bud)
}

function news(){
  if(modeNews){
    setInterval(function(){addThumb(getCoords(x),getCoords(y))},5000)
  }
}
function addThumb(x,y){
  var svgimg = d.createElementNS('http://www.w3.org/2000/svg','image');
  // var url = redditImg[0];
  svgimg.setAttributeNS(null,'width','100');
  svgimg.setAttributeNS(null,'height','100');
  svgimg.setAttribute('class','thumb');
  svgimg.setAttributeNS('http://www.w3.org/1999/xlink','href', 'img.png');
  svgimg.setAttributeNS(null,'x',x);
  svgimg.setAttributeNS(null,'y',y);
  svgimg.setAttributeNS(null, 'visibility', 'visible');
  svg.append(svgimg);
}

function cutTail(pathData,x,y){
  var pathArray = pathData.split(" ");
  pathArray.shift()
  pathArray.shift() // remove the 2 firsts coordinates
  newTail = pathArray[0].replace(/L/g, "M"); // change the L to an M for the new first coordonate
  pathArray.shift() // remove again the first coordinate of the array
  pathArray.unshift(newTail) // add the new tail at the begining of the array
  pathData = pathArray.join(' ')
  pathData += " L"+x+" "+y;
  return(pathData)
}


// // Reddit
// getJSONP( "https://www.reddit.com/r/comics/.json", function( data ) {
//   var redditImg = [];
//   $.each( data.data.children, function( i, obj ) {
//     if (checkURL(obj.data.url)) {
//       var itemTitle = obj.data.title;
//       var itemFullImgUrl = obj.data.url;
//       redditImg.push(itemFullImgUrl);
//     }
//   });
//   return redditImg
// });
//
// function checkURL(url) {
//   return(url.match(/\.(jpeg|jpg|gif|png)$/) != null);
// }
//
//
//
// function getJSONP(url, success) {
//   var ud = '_' + +new Date,
//     script = document.createElement('script'),
//     head = document.getElementsByTagName('head')[0]
//            || document.documentElement;
//    window[ud] = function(data) {
//     head.removeChild(script);
//     success && success(data);
//   };
//   script.src = url.replace('callback=?', 'callback=' + ud);
//   head.appendChild(script);
// }
