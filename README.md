# Each Page A Function

This webapp and its drawing tools are originally made for a drawing fanzine *Each Page A Function* by Raphaël Bastide edited by [Le mégot](http://lemegot.com/).

## Tips

- Prefer Chrome or Chromium to print this publication
- Allow access to the microphone in your browser to make the function audioTrembling() work properly

## Demo

[You can try it here](https://raphaelbastide.com/epaf/).

## License

[GPL](https://www.gnu.org/licenses/gpl.html)

## More

Made with [Paperjs](http://paperjs.org/)

For a simple SVG drawing tool, see https://gitlab.com/raphaelbastide/svg-simple-draw

Also:
- http://armansansd.net/magnet/
- http://recursivedrawing.com/
- http://maximecb.github.io/Turing-Drawings/
- https://scri.ch/
- https://turtletoy.net/
- http://drawingcurved.osp.kitchen/
